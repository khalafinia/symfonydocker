<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EditProfileController extends BaseController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/edit/profile", name="edit_profile" ,methods={"GET","POST"})
     */
    public function index(Request $request, ObjectManager $manager)
    {
        $result='';

        if ('POST' === $request->getMethod()) {
            $result = $this->editProfile($request, $manager);
        }

        return $this->render('edit_profile/home.html.twig', [
            'controller_name' => 'EditProfileController',
            'title' => 'Edit Profile',
            'content' => '$content',
            'links' => $this->getLinks(),
            'user' => $this->getUser(),
            'result' => $result,
        ]);
    }


    public function editProfile(Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();
        $message = '';

        if ($password = $request->request->get('oldPassword')) {
            if ($this->passwordEncoder->isPasswordValid($user, $password)) {
                if ($request->request->get('newPassword') === $request->request->get('newPasswordConfirm')) {
                    if (true === empty($request->request->get('newPassword'))){
                        return 'New Password can\'t be empty!';
                    }
                    $user->setPassword($this->passwordEncoder->encodePassword($user,
                        $request->request->get('newPassword')));
                } else {
                   return 'Password don\'t match!';
                }
            } else {
                return 'Password wrong!';
            }
            $message = 'Password Changed.';
        }
        if (true === empty($request->request->get('email'))){
            return 'Email can\'t be empty!';
        }
        $user->setEmail($request->request->get('email'));
        $manager->persist($user);
        $manager->flush();
        $message= 'Email Changed. '. $message;

        return $message;
    }
}
