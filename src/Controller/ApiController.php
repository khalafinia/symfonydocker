<?php


namespace App\Controller;


use App\Entity\ApiToken;
use App\Repository\ApiTokenRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends BaseController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {

        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/api/login", name="api_login" )
     *
     */
    public function loginWithGet()
    {
        return RedirectResponse::create('/home');
    }

    /**
     * @Route("/api/get_token",name="api_action_get_token", methods={"GET","POST"})
     * @param ObjectManager $manager
     * @param ApiTokenRepository $apiTokenRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     *
     * __description(Shows you your token, If the token is expired creates a new one,Available to role ROLE_API_USER)
     */
    public function apiGetToken(ObjectManager $manager, ApiTokenRepository $apiTokenRepository)
    {
        if (false === $this->isGranted('ROLE_API_GENERAL')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };

        $savedToken = $apiTokenRepository->findOneBy(
            array('user' => $this->getUser()),
            array('id' => 'DESC'));

        if (isset($savedToken) && $savedToken->getExpiresAt() > new \DateTime('now')) {
            return $this->json(
                $savedToken,
                Response::HTTP_OK,
                array(),
                array(
                    'groups' => array('publishable',)
                ));
        }

        $newToken = new ApiToken($this->getUser());
        $manager->persist($newToken);
        $manager->flush();

        return $this->json(
            $newToken,
            Response::HTTP_OK,
            array(),
            array(
                'groups' => array('publishable',)
            ));
    }

    /**
     * @Route("/api/get_users",name="api_action_get_users", methods={"GET","POST"})
     *
     * __description(Show a list of all users Emial and their Roles,Available to role ROLE_ADMIN)
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiGetUsers(UserRepository $userRepository)
    {
        if (false === $this->isGranted('ROLE_API_EDIT')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };

        return $this->json($userRepository->findAll(), Response::HTTP_OK, array(), array(
            'groups' => array('publishable',)
        ));
    }

    /**
     * @Route("/api/change_my_password",name="api_action_change_my_password", methods={"GET","POST"})
     * @param Request $request
     * @param ObjectManager $manager
     *
     * __description(Change Your own password,Available to role ROLE_ADMIN)
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiChangeMyPassword(Request $request, ObjectManager $manager)
    {
        if (false === $this->isGranted('ROLE_API_GENERAL')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };
        if ("POST" !== $request->getMethod()) {
            return $this->json(
                array('message' => 'Only POST method is accepted'),
                Response::HTTP_BAD_REQUEST
            );
        }
        $password = $request->request->get('password');

        if ($password) {
            $user = $this->getUser();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));

            $manager->persist($user);
            $manager->flush();
        }

        return $this->json(array('successful' => 'Password changed for :' . $user->getEmail()), Response::HTTP_OK,
            array(), array(
                'groups' => array('publishable',)
            ));
    }

    /**
     * @Route("/api/add_user_role",name="api_action_add_user_role", methods={"GET","POST"})
     *
     *  __description(Adds a role to user,Available to role ROLE_ADMIN)
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiAddUserRoles(Request $request, UserRepository $userRepository, ObjectManager $manager)
    {
        if (false === $this->isGranted('ROLE_API_EDIT')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };
        if ("POST" !== $request->getMethod()) {
            return $this->json(
                array('message' => 'Only POST method is accepted'),
                Response::HTTP_BAD_REQUEST
            );
        }
        $email = $request->request->get('email');
        $role = $request->request->get('role');
        $userFound = $userRepository->findOneBy(array('email' => $email));


        if (empty($userFound)) {
            return $this->json(
                array('message' => 'User Not Found'),
                Response::HTTP_BAD_REQUEST
            );
        }

        $result = $userFound->addRole($role);
        if ($result) {
            $manager->persist($userFound);
            $manager->flush();

            return $this->json($result['message'], $result['status']);
        } else {
            return $this->json(array('message' => 'something went wrong'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/api/remove_user_role",name="api_action_remove_user_role", methods={"GET","POST"})
     *
     * __description(Removes a role from the user, Available to role ROLE_ADMIN)
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiRemoveUserRoles(Request $request, UserRepository $userRepository, ObjectManager $manager)
    {
        // Returns notifying response when user is not granted
        if (false === $this->isGranted('ROLE_API_EDIT')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };
        // Returns notifying response when method is not POST
        if ("POST" !== $request->getMethod()) {
            return $this->json(
                array('message' => 'Only POST method is accepted'),
                Response::HTTP_BAD_REQUEST
            );
        }

        $email = $request->request->get('email');
        $role = $request->request->get('role');
        $userFound = $userRepository->findOneBy(array('email' => $email));

        if (empty($userFound)) {
            return $this->json(
                array('message' => 'User Not Found'),
                Response::HTTP_BAD_REQUEST
            );
        }

        $result = $userFound->removeRole($role);
        if ($result) {
            $manager->persist($userFound);
            $manager->flush();

            return $this->json($result['message'], $result['status']);
        } else {
            return $this->json(array('message' => 'something went wrong'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @Route("api/available_actions", name="api_action_routes", methods={"GET"})
     *
     * __description(Returns all available api actions,Available to role ROLE_API_USER)
     *
     * @return array|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function apiGetAllAvailableRoutes()
    {
        if (false === $this->isGranted('ROLE_API_GENERAL')) {
            return $this->json(
                array('message' => 'Access Denied - Check Your Role Preferences'),
                Response::HTTP_UNAUTHORIZED
            );
        };
        $routesInfo = $this->availableRoutes();

        return $this->json(
            $routesInfo,
            Response::HTTP_OK);

    }

    /**
     * @return array
     */
    protected function availableRoutes(): array
    {
        /** @var Router $router */
        $router = $this->get('router');
        $routes = $router->getRouteCollection();
        $functions = get_class_methods($this);

        $routesInfo = array();
        foreach ($functions as $function) {
            if (0 === strpos($function, 'api')) {
                $routeInfo = $this->getRouteDescription($function);
                $routesInfo[$routeInfo['routeName']]['Description'] = $routeInfo['description'];

            }
        }

        foreach ($routes->all() as $routeName => $info) {
            if (0 === strpos($routeName, 'api_action_')) {
                $routesInfo[$routeName]['Path'] = $info->getPath();
            }
        }
        return $routesInfo;
    }

    private function getRouteDescription($function)
    {
        $ReflectionClass = new \ReflectionClass($this);
        $docBlock = $ReflectionClass->getMethod($function)->getDocComment();

        $routeName = $this->findRouteName($docBlock);
        $description = '';
        if (strpos($docBlock, '__description')) {
            $description = (substr($docBlock,
                strpos(
                    $docBlock,
                    '__description') + 14,
                strpos($docBlock, ')', strpos($docBlock, '__description') + 14) - (strpos(
                        $docBlock,
                        '__description') + 14))
            );
        }
        return array('routeName' => $routeName, 'description' => $description);
    }

    private function findRouteName($docBlock)
    {
        if (strpos($docBlock, '@Route')) {
            return (substr($docBlock,
                strpos(
                    $docBlock,
                    'name="') + 6,
                strpos($docBlock, '"', strpos($docBlock, 'name="') + 6) - (strpos(
                        $docBlock,
                        'name="') + 6))
            );
        }
        return '';
    }

    /**
     * @Route("api/home", name="api_home", methods={"GET"})
     *
     *@return Response
     */
    public function home()
    {
        $content = 'WELCOME';
        return $this->render('api/home.html.twig', array(
            'title' => 'Home',
            'content' => $content,
            'links' => $this->getLinks(),
            'apiLinks' => $this->availableRoutes(),
        ));
    }
}