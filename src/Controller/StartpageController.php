<?php


namespace App\Controller;


use App\Services\AvailableLinksManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class StartpageController extends BaseController
{
    /**
     * @var AvailableLinksManager
     */
    private $linksClass;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(AvailableLinksManager $links,UrlGeneratorInterface $urlGenerator)
    {
        $this->linksClass = $links;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/",name="startpage")
     *
     * @return Response
     */
    public function startpage()
    {
        if ($this->isGranted('ROLE_USER')){
            return new RedirectResponse($this->urlGenerator->generate('home'));
        }
        return $this->render('startpage/startpage.html.twig', array(
            'title' => 'startpage',
            'links' => $this->getLinks(),
        ));
    }

}