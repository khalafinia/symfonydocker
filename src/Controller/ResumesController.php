<?php


namespace App\Controller;


use App\Entity\Resume;
use App\Form\AddResumeType;
use App\Repository\ResumeRepository;
use App\Services\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResumesController extends BaseController
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    /**
     * @Route("/add_resume",name="action_resumes_add", methods={"GET","POST"})
     *
     * @return Response
     */
    public function addResumeAction(ObjectManager $manager, Request $request, FileUploader $fileUploader)
    {

        $form = $this->createForm(AddResumeType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Resume $resume */
            $resume = $form->getData();
            $resume->setAddedByUser($this->getUser());

            $resumeFile = $form['file']->getData();
            if ($resumeFile) {
                $resumeFileName = $fileUploader->upload($resumeFile);
                $resume->setFile($resumeFileName);
            }

            $manager->persist($resume);
            $manager->flush();
        }
        return $this->render('Resumes/addResume.html.twig', array(
            'addResumeForm' => $form->createView(),
            'title' => 'Add Resume',
            'links' => $this->getLinks(),
        ));
    }

    /**
     * @Route("/list_resumes",name="action_resumes_list_all")
     *
     * @return Response
     */
    public function listAction(ResumeRepository $repository, Request $request)
    {
        $q = $request->query->get('q');
        if ($q) {
            $resumes = $repository->searchINNameEmailTagSourceFile($q);
        } else {
            $resumes = $repository->findAll();
        }

        return $this->render('Resumes/list.html.twig', array(
            'title' => 'List Of All Resumes',
            'resumes' => $resumes,
            'links' => $this->getLinks(),
        ));
    }

    /**
     * @Route("/list_resumes/download/{filename}",name="route_resumes_download",methods={"GET"})
     *
     * @return Response
     */
    public function download($filename)
    {
        $headers = array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="file1.pdf"'
        );
        $fileDirectory = $this->fileUploader->getTargetDirectory();

        if (false === is_file($fileDirectory . '/' . $filename)) {
            return new Response('File Not Found', 404);
        }
        return new Response(file_get_contents($fileDirectory . '/' . $filename), 200, $headers);

    }

    /**
     * @Route("resumes/edit/{resumeId}",name="route_resumes_edit", methods={"GET","POST"})
     *
     * @return Response
     */
    public function editResumeRoute(
        $resumeId,
        ResumeRepository $resumeRepository,
        ObjectManager $manager,
        Request $request,
        FileUploader $fileUploader
    ) {
        $resume = $resumeRepository->findOneBy(array('id' => $resumeId));
        $form = $this->createForm(AddResumeType::class, $resume);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Resume $resume */
            $resume = $form->getData();
            $resume->setAddedByUser($this->getUser());

            $resumeFile = $form['file']->getData();
            if ($resumeFile) {
                $resumeFileName = $fileUploader->upload($resumeFile);
                $resume->setFile($resumeFileName);
            }
            $manager->persist($resume);
            $manager->flush();
        }

        return $this->render('Resumes/edit.html.twig', array(
            'title' => 'Edit Resume',
            'links' => $this->getLinks(),
            'addResumeForm' => $form->createView(),
        ));
    }

    /**
     * @Route("resumes/add-to-resume/{field}/{resumeId}",name="route_resumes_add-to-resume", methods={"POST"})
     *
     * @return Response
     */
    public function addTagRoute($field,$resumeId, ResumeRepository $resumeRepository, ObjectManager $manager, Request $request)
    {
        if ($content = $request->getContent()) {
            try {
                $parametersAsArray = json_decode($content, true);
                $resume = $resumeRepository->findOneBy(array('id' => $resumeId));
                $param = $parametersAsArray[$field];
                $setString = 'set'.ucfirst($field);
                $resume->$setString($param);
                $manager->persist($resume);
                $manager->flush();
                return new JsonResponse($param, Response::HTTP_OK);
            } catch (\Exception $e) {
                return new JsonResponse("SERVER ERROR: " . $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            return new JsonResponse("Bad Request", Response::HTTP_BAD_REQUEST);
        }
    }
}