<?php


namespace App\Controller;


use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @Route("/home",name="home")
     * @param AdapterInterface $cache
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function home(AdapterInterface $cache)
    {
        $content = 'WELCOME';
        $item = $cache->getItem('content_'.md5($content));
        if (!$item->isHit()){
            $item->set($content);
            $cache->save($item);
        }
        $content = $item->get();
        return $this->render('home/home.html.twig', array(
            'title' => 'Home',
            'content'=>$content,
            'links' => $this->getLinks(),
        ));
    }


}