<?php


namespace App\Controller;


use App\Entity\User;
use App\Services\AvailableLinksManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{

    private $availableLinksManager;

    /**
     * @return array
     */
    public function getLinks()
    {
        $links = array(
            'headerLinks' => $this->getAvailableLinksManager()->headerLinks(),
            'footerLinks' => $this->getAvailableLinksManager()->footerLinks(),
            'leftMenuLinks' => $this->getAvailableLinksManager()->leftMenuLinks(),
        );

        return $links;
    }

    /**
     * @return AvailableLinksManager
     */
    public function getAvailableLinksManager(): AvailableLinksManager
    {
        return $this->availableLinksManager = new AvailableLinksManager($this->isGranted('ROLE_USER'));
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return parent::getUser();
    }


}