<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApiTokenRepository")
 */
class ApiToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("publishable")
     */
    private $token;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("publishable")
     */
    private $expiresAt;

    /**
     * @var string
     * @Groups("publishable")
     */
    public $expirationPeriod = '+12 hour';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="apiTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct(User $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->user = $user;
        $this->expiresAt = new \DateTime($this->expirationPeriod);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }


    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function expiresNow(){
        $this->expiresAt = new \DateTime('+12 hour');

    }

}
