<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups("publishable")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups("publishable")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ApiToken", mappedBy="user", orphanRemoval=true)
     */
    private $apiTokens;

    private $availableRoles = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_ROOT', 'ROLE_API_USER'];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Resume", mappedBy="addedByUser", orphanRemoval=true)
     */
    private $resumes;

    public function __construct()
    {
        $this->apiTokens = new ArrayCollection();
        $this->resumes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml

    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|ApiToken[]
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function addApiToken(ApiToken $apiToken): self
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens[] = $apiToken;
        }

        return $this;
    }

    /**
     * @param string $role
     * @return array
     */
    public function addRole($role)
    {
        if (false === in_array($role, $this->getAvailableRoles())) {
            return array(
                'message' => 'Given role is not allowed. Allowed are : ' . implode(' , ', $this->getAvailableRoles()),
                'status' => Response::HTTP_BAD_REQUEST
            );
        }

        $currentUserRoles = $this->getRoles();

        if (false === in_array($role, $currentUserRoles) && $role !== 'ROLE_USER') {
            array_push($currentUserRoles, $role);
            $this->setRoles($currentUserRoles);
            return array(
                'message' => sprintf('User %s has now the roles: %s', $this->email, implode(' , ', $this->getRoles())),
                'status' => Response::HTTP_OK
            );

        } else {
            return array(
                'message' => 'User already has this role',
                'status' => Response::HTTP_BAD_REQUEST,
            );
        }
    }

    /**
     * @param string $role
     * @return array
     */
    public function removeRole($role)
    {
        $currentUserRoles = $this->getRoles();

        if (false === in_array($role, $currentUserRoles) && $role !== 'ROLE_USER') {
            return array(
                'message' => 'User does not have this role',
                'status' => Response::HTTP_BAD_REQUEST,
            );
        } else {
            unset ($currentUserRoles[array_search($role,$currentUserRoles)]);
            $this->setRoles($currentUserRoles);
            return array(
                'message' => sprintf('User %s has now the roles: %s', $this->email, implode(' , ', $this->getRoles())),
                'status' => Response::HTTP_OK
            );
        }
    }

    /**
     * @return array
     */
    public function getAvailableRoles(): array
    {
        return $this->availableRoles;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return Collection|Resume[]
     */
    public function getResumes(): Collection
    {
        return $this->resumes;
    }

    public function addResume(Resume $resume): self
    {
        if (!$this->resumes->contains($resume)) {
            $this->resumes[] = $resume;
            $resume->setAddedByUser($this);
        }

        return $this;
    }

    public function removeResume(Resume $resume): self
    {
        if ($this->resumes->contains($resume)) {
            $this->resumes->removeElement($resume);
            // set the owning side to null (unless already changed)
            if ($resume->getAddedByUser() === $this) {
                $resume->setAddedByUser(null);
            }
        }

        return $this;
    }

}
