<?php


namespace App\Services;


use Psr\Log\LoggerInterface;

class ApiLogger
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function logit($message){
        $this->logger->info($message);
    }
}
