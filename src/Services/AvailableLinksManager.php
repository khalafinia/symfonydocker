<?php


namespace App\Services;


class AvailableLinksManager
{


    /**
     * @var bool
     */
    private $isLoggedIn;

    public function __construct(bool $isLoggedIn)
    {
        $this->isLoggedIn = $isLoggedIn;
    }

    /**
     * @return array
     */
    public function footerLinks()
    {
        //todo implement public vs user link logic
        return array('Contact' => '#', 'About' => '#');
    }

    /**
     * @return array
     */
    public function headerLinks()
    {
        $links = array();
        //todo implement public vs user link logic
        if ($this->isLoggedIn) {
            $links['Edit Your Profile'] = 'edit_profile';
            return $links;
        }

        $links['Sign In / Register'] = 'login';

        return $links;

    }

    public function leftMenuLinks()
    {
        //todo implement public vs user link logic
        return array(
            'API UI' => 'api_home',
            'Add New Resume' => 'action_resumes_add',
            'List All Resumes' => 'action_resumes_list_all',
        );
    }


}