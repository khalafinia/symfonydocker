<?php

namespace App\DataFixtures;

use App\Entity\Resume;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResumeFixture extends BaseFixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {

        for($i=0;$i<10;$i++){

            $user = new User();
            $user->setEmail(sprintf('applicant_adder%d@test.com',$i));
            $user->setPassword($this->passwordEncoder->encodePassword($user,'test'));
            $manager->persist($user);
            $resume = new Resume();
            $resume->setEmail(sprintf('applicant%d@test.com',$i));
            $resume->setName($this->faker->name);
            $resume->setLastName($this->faker->lastName);
            $resume->setImportedAt();
            $resume->setReviewed(false);
            $resume->setAddedByUser($user);
            $resume->setFile('test.pdf');
            $manager->persist($resume);
        };

        $manager->flush();
    }
}
