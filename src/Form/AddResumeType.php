<?php

namespace App\Form;

use App\Entity\Resume;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class AddResumeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('lastName')
            ->add('email')
//            ->add('importedAt',null,array(
//                'widget' => 'single_text',
//                'help' => 'The date now',
//                'attr' => array('class'=>'test')
//            ))
            ->add('source')
            ->add('file',FileType::class,array(
                'label' => 'Resume (PDF file)',
                'attr'=>array(
                    'onchange' =>"this.nextSibling.innerHTML=this.value.replace('C:\\\\fakepath\\\','')"
                ),

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => array(
                    new File(array(
                        'maxSize' => '10024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ))
                ),
            ))
            ->add('tag')
            ->add('links')
            ->add('employmentHistory')
            ->add('educationHistory')
            ->add('reviewed')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Resume::class,
        ]);
    }
}
