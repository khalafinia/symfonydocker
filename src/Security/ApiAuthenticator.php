<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;


class ApiAuthenticator extends AbstractGuardAuthenticator
{

    private $apiTokenRepository;

    private $urlGenerator;

    public function __construct(
        ApiTokenRepository $apiTokenRepository,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->apiTokenRepository = $apiTokenRepository;
        $this->urlGenerator = $urlGenerator;
    }

    public function supports(Request $request)
    {
//        dd($request->getPathInfo());
        // For Post Requests
        $condition = $request->headers->has('Authorization')
            && 0 === strpos($request->headers->get('Authorization'), 'Bearer ');
        // For Get Requests
        $condition = $condition || '/api/login' === $request->getPathInfo();
        return $condition;
    }

    public function getCredentials(Request $request)
    {
        $authorizationHeader = $request->headers->get('Authorization');

        if ($request->get('token')){
            return $request->get('token');
        }

        return substr($authorizationHeader, 7);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $this->apiTokenRepository->findOneBy(array('token' => $credentials));
        if ($token){
            return $token->getUser();
        }else{
            throw new \Exception('Token Error: Please check your token');
        }


    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $token = $this->apiTokenRepository->findOneBy(array('token' => $credentials));

        if (!$token) {
            //todo Define api specific exception listner and handler
            throw new \Exception('Wrong Credentials');
        }
        if ($token->getExpiresAt() < new \DateTime('now')) {
            //todo Define api specific exception listner and handler
            throw new \Exception('Expired Credentials');
        }

        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // todo
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // todo
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        // todo
    }

    public function supportsRememberMe()
    {
        // todo
    }
}
