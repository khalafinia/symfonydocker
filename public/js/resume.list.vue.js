var resumeVue = new Vue({
    el: '#resume-list',
    delimiters: ["<%", "%>"],
    data: {},
    methods: {
        showDetails: function (event) {
            if (event.currentTarget.nextSibling.style.display === 'none') {
                event.currentTarget.nextSibling.style.display = 'table-row';
                event.currentTarget.firstChild.firstChild.className = 'fa fa-angle-up'
            } else {
                event.currentTarget.nextSibling.style.display = 'none';
                event.currentTarget.firstChild.firstChild.className = 'fa fa-angle-down'
            }
        },
        setCaretPositionToEnd: function (elem) {
            if (elem != null) {
                if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', 9999999);
                    range.select();
                } else {
                    if (elem.selectionStart) {
                        elem.focus();
                        elem.setSelectionRange(9999999, 9999999);
                    } else
                        elem.focus();
                }
            }
        },
        enterKeyBlurs: function (e, elm) {
            let key = e.which || e.keyCode;
            if (key === 13 && !e.shiftKey) {
                elm.blur()
            }
        },
        addToResume: function (event, url,param) {
            let currentTag = event.currentTarget.innerHTML;
            if ($(event.target).is('input')) return;
            let inputForm = document.createElement('input');
            inputForm.className = 'fitParent';
            inputForm.type = 'text';
            inputForm.value = currentTag;
            inputForm.addEventListener('blur', function (e) {
                resumeVue.addToResumeField(this.value, e, url,param);
            });
            inputForm.addEventListener('keypress', function (e) {
                resumeVue.enterKeyBlurs(e, this)
            });
            this.setCaretPositionToEnd(inputForm);
            event.currentTarget.append(inputForm);
            inputForm.focus();
        },
        addToResumeField: function (v, event, url,param) {
            let element = event.currentTarget.parentElement;
            let bodyObj = {};
            bodyObj[param]=v;
            fetch(url, {
                body: JSON.stringify(bodyObj),
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then(response => response.json())
                .then(function (data) {
                    element.innerHTML = data;
                    element.disabled = false;
                })
                .catch(function (e) {
                    alert('Something Went wrong' + e);
                });
            element.disabled = true;
            console.log(event.currentTarget.parentElement);
        },
    }
});