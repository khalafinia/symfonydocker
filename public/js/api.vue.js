const vueBody = new Vue({
    el: '#vueBody',
    delimiters: ["<%", "%>"],
    data: {
        returnedData: '',
    },
    methods: {
        getContent: function (href) {
            vueBody.returnedData = 'LOADING....';
            fetch(href,
                {
                    body: JSON.stringify(),
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            ).catch( reason => console.log(reason)

            ).then(response => response.json()).then(function (data) {
                console.log(data);
                vueBody.returnedData = data;
            });
            console.log(this.returnedData);
        },
    },

});