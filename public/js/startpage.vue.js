var app = new Vue({
    el: '#startpage',
    data: {
        message: 'Hello Vue!',
        signInBoxVisible: true,
    },
    methods : {
        showSignIn: function () {
            this.signInBoxVisible = true;
        },
        showRegister: function () {
            this.signInBoxVisible = false;
        }
    }
});